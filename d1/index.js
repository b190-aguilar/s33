// SECTION - JS Synchronous vs Asynchronous
/*
	JS is Synchronous, meaning that only one statement will be executed at a time, starting from top to bottom.
*/
/*
console.log("Hello World!");
conosle.log("Hello Again!"); //Shows an error, JS will not proceed to the next statement.
console.log("Goodbye!");
*/

// When statements take time to process, this slows down our code
	// ex. when loops are used in a large amount of information, or when fetching data from databases.

// We might not notice it due to the fast processing power of our devices.
// This is the reason why some websites don't instantly load, and we only see a white screen at times while the application is waiting for all the code to be executed.
/*
for (let i = 1; i <= 1500; i++){
	console.log(i);
};

console.log("It's me again!");
*/





// SECTION - ASYNCHRONOUS
/*
	ASYNCHRONOUS - allows executing of codes simultaneously prioritizing on the less resource-consuming codes while the more complex and more resource-consuming codes run at the back.
*/

// FETCH API - allows us to asynchronously request for a resource (data).
// a "promise" is an object that represents the EVENTUAL completion (in some cases, failure) of an asynchronous function in its resulting value.
/*
	SYNTAX:
		fetch("url");

*/

// console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

// retrieves all posts following the REST API (retrieve, /posts, GET)
fetch("https://jsonplaceholder.typicode.com/posts")
// by using the ".then" method, we can now check the status of the prompts.
// "fetch" method will return a "promise" that resolves to a response object.
//  the ".then" method captures the response object and returns another "promise" which will eventually be resolved or rejected.


// .then(response => console.log(response.status));


// we use the "json" method from the response object to convert the data retrieved into JSON format to be used in our application.
.then(response => response.json())
// using multiple ".then" methods will result into "promise chain"
// then we can now access it and log in the console our desired output.
.then(json => console.log(json));


console.log("Hello");
console.log("Hello World");
console.log("Hello Again");


// ASYNC-AWAIT

// "async" and "await" keywords are other approach that can be used to perform asynchronous javascript
// used in functions to indicate which portions of the code should be waited for


// creates and asynchronous option
async function fetchData(){
	// waits for the fetch method to complete, then stores it inside the "result" variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	// result of returned fetch from the "result" variable
	console.log(result);
	// the returned value is an object
	console.log(typeof result);
	// we cannot access the content of the response object by directly accessing the body (need to jsonify)
	console.log(result.body);

	// converts the data from the response object inside the result variable as JSON
	let json = await result.json();
	// prints out the content of the response object
	console.log(json);
};


fetchData();
// this will execute first
console.log("Henlo!");



// SECTION: Creating a post
/*
	SYNTAX:
		fetch(URL, options)

*/

// create a new post following REST IP (create, /posts, POST)
fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST",
	// setting request headers to be sent to the backend
	// specified that the request headers will be sending JSON structure for its content
	headers: {
		"Content-Type": "application/json"
	},
	// set content/body data of the "request" object to be sent to the backend
	// JSON.stringify converts the object into stringify JSON
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));


/*
	with this link, https://jsonplaceholder.typicode.com/posts
	perform PUT method with the following request context 
		header: content type = application
		id: 1
		title: Updated Post
		body: Hello Again
		userId: 1

	send output in google chat
*/



// SECTION - Updating Post (PUT)
	// updates a specific post following the REST API (update, /posts/:id, PUT)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated Post",
		body: "Hello Again",
		userId: 1
	})

})
.then(response => response.json())
.then(json => console.log(json));




// SECTION - Updating Post (PATCH)
/*
	the difference between PUT and PATCH is the number of properties being updated
	PATCH - is used to update a single property while maintaining the unupdated properties
	PUT - is used when ALL of the properties need to be updated, or the whole document itself
*/
	// updates a specific post following the REST API (update, /posts/:id, PATCH)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected Post",
	})

})
.then(response => response.json())
.then(json => console.log(json));



// SECTION - Deleting of a resource (DELETE)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
})
.then(response => response.json())
.then(json => console.log(json));





// SECTION - Filter Posts
	// the data can be filtered by sending the userId along with the URL
	// information sent via the url can be done by adding the question mark symbol (?)
/*
	SYNTAX:
		<url>?parameterName=value

		multiple paramaters using &&

		<url>?parameterA=valueA&&parameterB=valueB

*/

fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then(response => response.json())
.then(json => console.log(json));




// Retrieving comments for a specific post/accessing nested/ embedded comments
fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then(response => response.json())
.then(json => console.log(json));
