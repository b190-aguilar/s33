console.log("Hello World!");


// Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "GET"
})
.then(response => response.json())
.then(json => console.log(json));


//  Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "GET"
})
.then(response => response.json())
.then(json => console.log(json));


// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		id: 201,	
		title: "Created To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));



// Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id: 1,	
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));


// Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		status: "Complete",
	})
})
.then(response => response.json())
.then(json => console.log(json));


// Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})
.then(response => response.json())
.then(json => console.log(json));

